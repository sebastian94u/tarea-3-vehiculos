using System;
using System.IO;

namespace Pruebas
{
  public class SingletonPruebas
  {
    private static SingletonPruebas _instanciaPruebas;
    private int _contadorPruebas = 0;

    private SingletonPruebas() { }

    public string Pruebas(string pruebasRealizadas)
    {
      Console.WriteLine("*Pruebas*");
      GenerarArchivo(pruebasRealizadas);
      return $"{Acelerar()}\n{Avanzar()}\n{Frenar()}\n{Retroceder()}";
    }

    private string Acelerar()
    {

      return "Realizando Prueba: Acelerar.";
    }

    private string Avanzar()
    {

      return "Realizando Prueba: Avanzar.";
    }

    private string Frenar()
    {

      return "Realizando Prueba: Frenar.";
    }

    private string Retroceder()
    {

      return "Realizando Prueba: Retroceder.";
    }

    private void GenerarArchivo(string pruebasRealizadas)
    {
      string currentDirectory = Directory.GetCurrentDirectory();
      string dirPath = Path.Combine(currentDirectory, "pruebas-logs");
      string fileName = $"pruebas_{DateTime.Now:yyyyMMdd_HH_mm}.txt";
      string filePath = Path.Combine(dirPath, fileName);

      Console.WriteLine(dirPath);
      try
      {
        Directory.CreateDirectory(dirPath);
        File.Create(filePath).Close();
        File.WriteAllText(filePath, pruebasRealizadas);
      }
      catch (Exception e)
      {
        Console.WriteLine("Error creando archivo.");
      }
    }

    public static SingletonPruebas GetInstanciaPruebas()
    {
      if (_instanciaPruebas == null)
      {
        _instanciaPruebas = new SingletonPruebas();
      }
      return _instanciaPruebas;
    }
  }
}