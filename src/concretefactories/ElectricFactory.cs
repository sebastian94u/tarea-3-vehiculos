namespace Vehiculos
{
    public class ElectricFactory : AbstractVehiculo
    {
        public IAutomovil CreateAutomovil()
        {
            return new ConcreteAutomovilElectrico();
        }
        public IMotocicleta CreateMotocicleta()
        {
            return new ConcreteMotocicletaElectrica();
        }
        public IBicicleta CreateBicicleta()
        {
            return new ConcreteBicicletaElectrica();
        }
    }
}