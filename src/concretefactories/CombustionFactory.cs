namespace Vehiculos
{
    public class CombustionFactory : AbstractVehiculo
    {
        public IAutomovil CreateAutomovil()
        {
            return new ConcreteAutomovilCombustion();
        }
        public IMotocicleta CreateMotocicleta()
        {
            return new ConcreteMotocicletaCombustion();
        }
        public IBicicleta CreateBicicleta()
        {
            return new ConcreteBicicletaCombustion();
        }
    }
}