namespace Vehiculos
{
    public interface IAutomovil
    {
        string AcelerarAutomovil();
        string AvanzarAutomovil();
        string FrenarAutomovil();
        string RetrocederAutomovil();
    }
}