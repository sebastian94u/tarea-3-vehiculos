namespace Vehiculos
{
    public interface IBicicleta
    {        
        string AcelerarBicicleta();
        string AvanzarBicicleta();
        string FrenarBicicleta();
        string RetrocederBicicleta();
    }
}