namespace Vehiculos
{
    public interface IMotocicleta
    {        
        string AcelerarMotocicleta();
        string AvanzarMotocicleta();
        string FrenarMotocicleta();
        string RetrocederMotocicleta();
    }
}