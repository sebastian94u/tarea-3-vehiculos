namespace Vehiculos
{
    public interface AbstractVehiculo
    {
        IAutomovil CreateAutomovil();
        IMotocicleta CreateMotocicleta();
        IBicicleta CreateBicicleta();        
    }
}