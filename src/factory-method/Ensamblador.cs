using Vehiculos;
using Pruebas;

namespace Factory
{
  public class Ensamblador
  {
    AbstractVehiculo factory;
    SingletonPruebas pruebas;
    private int _opcionV;
    private int _opcionT;
    private bool estaDentro = true;

    public void FabricarVehiculo()
    {
      while (estaDentro)
      {
        Console.WriteLine("Ingresar vehiculo:\n1. Automovil.\n2. Motocicleta.\n3. Bicicleta");
        _opcionV = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Electrico (1), combustion (2), Salir (3)?");
        _opcionT = Convert.ToInt32(Console.ReadLine());
        switch (_opcionT)
        {
          case 1:
            Console.WriteLine("Seleccionaste la opción 1");
            FabricaElectricos(_opcionV);
            break;
          case 2:
            Console.WriteLine("Seleccionaste la opción 2");
            FabricaCombustion(_opcionV);
            break;
          case 3:
            Console.WriteLine("Salir");
            estaDentro = !estaDentro;
            break;
          default:
            Console.WriteLine("Opción no válida");
            break;
        }
      }
    }

    private void FabricaElectricos(int opcionVehiculo)
    {
      switch (opcionVehiculo)
      {
        case 1:
          EnsamblarAutomovil(new ElectricFactory(), opcionVehiculo);
          break;
        case 2:
          EnsamblarMotocicleta(new ElectricFactory(), opcionVehiculo);
          break;
        case 3:
          EnsamblarBicicleta(new ElectricFactory(), opcionVehiculo);
          break;
        default:
          Console.WriteLine("Opción no válida");
          break;
      }
    }
    private void FabricaCombustion(int opcionVehiculo)
    {
      switch (opcionVehiculo)
      {
        case 1:
          EnsamblarAutomovil(new CombustionFactory(), opcionVehiculo);
          break;
        case 2:
          EnsamblarMotocicleta(new CombustionFactory(), opcionVehiculo);
          break;
        case 3:
          EnsamblarBicicleta(new CombustionFactory(), opcionVehiculo);
          break;
        default:
          Console.WriteLine("Opción no válida");
          break;
      }
    }
    private void EnsamblarAutomovil(AbstractVehiculo factory, int opcionVehiculo)
    {
      var auto = factory.CreateAutomovil();
      string pruebasRealizadas = $"1. {auto.AcelerarAutomovil()}\n2. {auto.AvanzarAutomovil()}\n3. {auto.FrenarAutomovil()}\n4. {auto.RetrocederAutomovil()}";

      pruebas = SingletonPruebas.GetInstanciaPruebas();
      Console.WriteLine(pruebas.Pruebas(pruebasRealizadas));
    }
    private void EnsamblarMotocicleta(AbstractVehiculo factory, int opcionVehiculo)
    {
      var moto = factory.CreateMotocicleta();
      string pruebasRealizadas = $"1. {moto.AcelerarMotocicleta()}\n2. {moto.AvanzarMotocicleta()}\n3. {moto.FrenarMotocicleta()}\n4. {moto.RetrocederMotocicleta()}";

      pruebas = SingletonPruebas.GetInstanciaPruebas();
      Console.WriteLine(pruebas.Pruebas(pruebasRealizadas));
    }
    private void EnsamblarBicicleta(AbstractVehiculo factory, int opcionVehiculo)
    {
      var bici = factory.CreateBicicleta();
      string pruebasRealizadas = $"1. {bici.AcelerarBicicleta()}\n2. {bici.AvanzarBicicleta()}\n3. {bici.FrenarBicicleta()}\n4. {bici.RetrocederBicicleta()}";

      pruebas = SingletonPruebas.GetInstanciaPruebas();
      Console.WriteLine(pruebas.Pruebas(pruebasRealizadas));
    }
  }
}
