namespace Vehiculos
{
    public class ConcreteAutomovilElectrico : IAutomovil
    {
        public string AcelerarAutomovil()
        {
            return "Se hace la prueba de aceleración del automóvil eléctrica";
        }

         public string FrenarAutomovil()
        {
            return "Se hace la prueba de frenado del automóvil eléctrica";
        }

         public string RetrocederAutomovil()
        {
            return "Se hace la prueba de retroceso del automóvil eléctrica";
        }

         public string AvanzarAutomovil()
        {
            return "Se hace la prueba de avance del automóvil eléctrica";
        }
    }
}