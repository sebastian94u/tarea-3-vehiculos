namespace Vehiculos
{
    public class ConcreteMotocicletaCombustion : IMotocicleta
    {
        public string AcelerarMotocicleta()
        {
            return "Se hace la prueba de aceleración de la motocicleta de combustión";
        }

         public string FrenarMotocicleta()
        {
            return "Se hace la prueba de frenado de la motocicleta de combustión";
        }

         public string RetrocederMotocicleta()
        {
            return "Se hace la prueba de retroceso de la motocicleta de combustión";
        }

         public string AvanzarMotocicleta()
        {
            return "Se hace la prueba de avance de la motocicleta de combustión";
        }
    }
}