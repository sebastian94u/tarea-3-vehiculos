namespace Vehiculos
{
    public class ConcreteBicicletaCombustion : IBicicleta
    {
        public string AcelerarBicicleta()
        {
            return "Se hace la prueba de avance del bicicleta de combustión";
        }

         public string FrenarBicicleta()
        {
            return "Se hace la prueba de frenado del bicicleta de combustión";
        }

         public string RetrocederBicicleta()
        {
            return "Se hace la prueba de retroceso del bicicleta de combustión";
        }

         public string AvanzarBicicleta()
        {
            return "Se hace la prueba de avance del bicicleta de combustión";
        }
    }
}