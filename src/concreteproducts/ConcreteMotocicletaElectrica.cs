namespace Vehiculos
{
    public class ConcreteMotocicletaElectrica : IMotocicleta
    {
        public string AcelerarMotocicleta()
        {
            return "Se hace la prueba de aceleración de la motocicleta eléctrica";
        }

         public string FrenarMotocicleta()
        {
            return "Se hace la prueba de frenado de la motocicleta eléctrica";
        }

         public string RetrocederMotocicleta()
        {
            return "Se hace la prueba de retroceso de la motocicleta eléctrica";
        }

         public string AvanzarMotocicleta()
        {
            return "Se hace la prueba de avance de la motocicleta eléctrica";
        }
    }
}