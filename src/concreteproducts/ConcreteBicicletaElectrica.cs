namespace Vehiculos
{
    public class ConcreteBicicletaElectrica : IBicicleta
    {
        public string AvanzarBicicleta()
        {
            return "Se hace la prueba de aceleración de la bicicleta eléctrica.";
        }

         public string FrenarBicicleta()
        {
            return "Se hace la prueba de frenado de la bicicleta eléctrica.";
        }

         public string RetrocederBicicleta()
        {
            return "Se hace la prueba de retroceso de la bicicleta eléctrica.";
        }

         public string AcelerarBicicleta()
        {
            return "Se hace la prueba de avance de la bicicleta eléctrica.";
        }
    }
}