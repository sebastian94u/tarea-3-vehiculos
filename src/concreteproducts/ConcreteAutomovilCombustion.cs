namespace Vehiculos
{
    public class ConcreteAutomovilCombustion : IAutomovil
    {
        public string AcelerarAutomovil()
        {
            return "Se hace la prueba de aceleración del automóvil de combustión";
        }

         public string FrenarAutomovil()
        {
            return "Se hace la prueba de frenado del automóvil de combustión";
        }

         public string RetrocederAutomovil()
        {
            return "Se hace la prueba de retroceso del automóvil de combustión";
        }

         public string AvanzarAutomovil()
        {
            return "Se hace la prueba de avance del automóvil de combustión";
        }
    }
}