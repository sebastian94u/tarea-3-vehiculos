﻿using Pruebas;
using Vehiculos;
using Factory;

namespace Vehiculo
{
  internal class Program
  {
    public static void Main(string[] args)
    {
        // SingletonPruebas s1 = SingletonPruebas.GetInstanciaPruebas();
        // Console.WriteLine(s1.Acelerar());
        // Console.WriteLine(s1.Avanzar());
        // Console.WriteLine(s1.Frenar());
        // Console.WriteLine(s1.Retroceder());

        Ensamblador ensamblador = new Ensamblador();
        ensamblador.FabricarVehiculo();
    }
  }
}
